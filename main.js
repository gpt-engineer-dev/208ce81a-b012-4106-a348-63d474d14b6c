document.addEventListener('DOMContentLoaded', () => {
  const addTaskButton = document.getElementById('add-task');
  const newTaskInput = document.getElementById('new-task');
  const taskList = document.getElementById('task-list');

  addTaskButton.addEventListener('click', () => {
    const taskText = newTaskInput.value.trim();
    if (taskText) {
      const listItem = document.createElement('li');
      listItem.className = 'flex items-center justify-between bg-white px-4 py-2 rounded shadow mb-2';
      
      listItem.innerHTML = `
        <input type="checkbox" class="mr-2" onclick="toggleTaskStatus(this)">
        <span class="flex-1">${taskText}</span>
        <button onclick="deleteTask(this)" class="text-red-500 hover:text-red-700">
          <i class="fas fa-trash-alt"></i>
        </button>
      `;
      
      taskList.appendChild(listItem);
      newTaskInput.value = '';
    }
  });

  window.deleteTask = (button) => {
    button.parentElement.remove();
  };

  window.toggleTaskStatus = (checkbox) => {
    const taskText = checkbox.nextElementSibling;
    if (checkbox.checked) {
      taskText.classList.add('line-through', 'text-gray-500');
    } else {
      taskText.classList.remove('line-through', 'text-gray-500');
    }
  };
});
